package io.keiji.poker;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Random;


public class MyActivity extends ActionBarActivity {

    int bet_d = 0, bet_p = 0, life_p = 10, life_d = 10, flg = 0, bet_f = 0, pot = 0, flg2 = 0,
            pnt_d = 0, pnt_p = 0, cnt_b = 0, deck, roy_p, kind_d, kind_p, diff_kd,
            diff_kp, high_kd, high_kp , fls_d, fls_p, st_d, st_p, pair_d, pair_p, cnt_d, cnt_p,
            shuffle[], illust[], trump[], number[], hand_dsh[], hand_di[], hand_dn[],
            hand_psh[], hand_pn[], hand_pi[], cng[] = {0, 0, 0, 0, 0}, cng2[],
            com_d[], com_p[], diff_pd[], diff_pp[];

    String suit[], hand_ds[], hand_ps[], cdhs[] = {
        "c", "d", "h", "s"
    };

    ImageView d1, d2, d3, d4, d5, p1, p2, p3, p4, p5;
    TextView d_life, p_life, d_bet, p_bet;
    Button start, change, check, bet, falled;

    Toast toast;

    // 山札をタッチするとシャッフルするメソッド
    public void onShuffle(View view) {
        int ran1, ran2, buff;
        Random rnd1 = new Random(), rnd2 = new Random();
        trump = new int[52];
        suit = new String[52];
        for (int i = 0; i < 52; i++) {
            shuffle[i] = i;
        }

        start.setEnabled(false);

        // ランダムに値を入れ替える(シャッフル)
        for (int i = 0; i < 200; i++) {
            ran1 = rnd1.nextInt(52);
            ran2 = rnd2.nextInt(52);

            buff = shuffle[ran1];
            shuffle[ran1] = shuffle[ran2];
            shuffle[ran2] = buff;
        }

        // シャッフルした順番通りに配列に画像や数字、スートを代入
        for (int i = 0; i < 52; i++) {
            trump[i] = illust[shuffle[i]];
            number[i] = (shuffle[i]) % 13 + 1;
            suit[i] = cdhs[(shuffle[i]) / 13];
        }

        onDraw(view);
    }

    public void onDraw(View view) {
        hand_dsh = new int[5];
        hand_dn = new int[5];
        hand_ds = new String[5];
        hand_di = new int[5];
        hand_psh = new int[5];
        hand_pn = new int[5];
        hand_ps = new String[5];
        hand_pi = new int[5];

        // カードをドローして手札の情報を配列に代入
        for (int i = 0; i < 5; i++) {
            hand_dsh[i] = shuffle[i * 2];
            hand_dn[i] = number[i * 2];
            hand_ds[i] = suit[i * 2];
            hand_di[i] = trump[i * 2];
            hand_psh[i] = shuffle[i * 2 + 1];
            hand_pn[i] = number[i * 2 + 1];
            hand_ps[i] = suit[i * 2 + 1];
            hand_pi[i] = trump[i * 2 + 1];
        }

        p1.setImageResource(hand_pi[0]);
        p2.setImageResource(hand_pi[1]);
        p3.setImageResource(hand_pi[2]);
        p4.setImageResource(hand_pi[3]);
        p5.setImageResource(hand_pi[4]);

        check.setEnabled(true);
        bet.setEnabled(true);
        falled.setEnabled(true);

        toast = Toast.makeText(this, "1回目のベッティングラウンドです", Toast.LENGTH_LONG);
        toast.show();
    }

    public void onBet(View view) {
        check.setEnabled(false);
        bet.setEnabled(false);
        falled.setEnabled(false);

        int lose = 0;
        String tag = view.getTag().toString();
        switch (tag) {
            case "check" :
                if (cnt_b != 0) {
                    pot += (bet_d - bet_p);
                    life_p -= (bet_d - bet_p);
                    bet_p += (bet_d - bet_p);
                }
                break;
            case "bet" :
                if (cnt_b == 0) {
                    pot++;
                    life_p--;
                    bet_p++;
                } else {
                    pot += (bet_d - bet_p) + 1;
                    life_p -= (bet_d - bet_p) + 1;
                    bet_p += (bet_d - bet_p) + 1;
                }
                cnt_b++;
                break;
            case "falled" :
                pot++;
                life_p--;
                lose = 1;
                onLose(view);
                break;
        }
        p_life.setText(String.valueOf(life_p));
        p_bet.setText(String.valueOf(bet_p));

        if (cnt_b != 0 && bet_d == bet_p) {
            if (bet_f == 0) {
                if (bet_d == bet_p) {
                    bet_f = 1;
                    change.setEnabled(true);
                    p1.setEnabled(true);
                    p2.setEnabled(true);
                    p3.setEnabled(true);
                    p4.setEnabled(true);
                    p5.setEnabled(true);

                    if (toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(this,
                            "チェンジングラウンドです\n交換したい手札を\nタップしてください",
                            Toast.LENGTH_LONG);
                    toast.show();
                    return;
                } else {
                    check.setEnabled(true);
                    bet.setEnabled(true);
                    falled.setEnabled(true);
                }
            } else {
                if (bet_d == bet_p) {
                    flg2 = 1;
                    onCheck(view);
                    return;
                } else {
                    check.setEnabled(true);
                    bet.setEnabled(true);
                    falled.setEnabled(true);
                }
            }
        }

        if (lose == 0) {
            // 現状ディーラーにはベットもしくはコールのみをさせる
            if (cnt_b == 0) {
                pot++;
                life_d--;
                bet_d++;
                cnt_b++;
                check.setText(R.string.call);
                bet.setText(R.string.raise);
            } else {
                pot += (bet_p - bet_d);
                life_d -= (bet_p - bet_d);
                bet_d += (bet_p - bet_d);
            }
            d_life.setText(String.valueOf(life_d));
            d_bet.setText(String.valueOf(bet_d));
            if (toast != null) {
                toast.cancel();
            }

            if (bet_f == 0) {
                if (bet_d == bet_p) {
                    bet_f = 1;
                    change.setEnabled(true);
                    p1.setEnabled(true);
                    p2.setEnabled(true);
                    p3.setEnabled(true);
                    p4.setEnabled(true);
                    p5.setEnabled(true);

                    toast = Toast.makeText(this,
                            "相手はコールしました\nチェンジングラウンドです\n" +
                                    "交換したい手札を\nタップしてください",
                            Toast.LENGTH_LONG);
                } else {
                    check.setEnabled(true);
                    bet.setEnabled(true);
                    falled.setEnabled(true);

                    toast = Toast.makeText(this, "相手はベットしました", Toast.LENGTH_LONG);
                }
            } else {
                if (bet_d == bet_p) {
                    flg2 = 1;
                    onCheck(view);
                } else {
                    check.setEnabled(true);
                    bet.setEnabled(true);
                    falled.setEnabled(true);

                    toast = Toast.makeText(this, "相手はベットしました", Toast.LENGTH_LONG);
                }
            }
            toast.show();
        }
    }

    // 手札入れ替え選択
    public void onChoose(View view) {
        String tag = view.getTag().toString();
        switch (tag) {
            case "p1" :
                if (cng[0] == 0) {
                    cng[0] = 1;
                    p1.setImageResource(R.drawable.z01);
                } else {
                    cng[0] = 0;
                    p1.setImageResource(hand_pi[0]);
                }
                break;
            case "p2" :
                if (cng[1] == 0) {
                    cng[1] = 1;
                    p2.setImageResource(R.drawable.z01);
                } else {
                    cng[1] = 0;
                    p2.setImageResource(hand_pi[1]);
                }
                break;
            case "p3" :
                if (cng[2] == 0) {
                    cng[2] = 1;
                    p3.setImageResource(R.drawable.z01);
                } else {
                    cng[2] = 0;
                    p3.setImageResource(hand_pi[2]);
                }
                break;
            case "p4" :
                if (cng[3] == 0) {
                    cng[3] = 1;
                    p4.setImageResource(R.drawable.z01);
                } else {
                    cng[3] = 0;
                    p4.setImageResource(hand_pi[3]);
                }
                break;
            case "p5" :
                if (cng[4] == 0) {
                    cng[4] = 1;
                    p5.setImageResource(R.drawable.z01);
                } else {
                    cng[4] = 0;
                    p5.setImageResource(hand_pi[4]);
                }
                break;
        }
    }

    // 手札入れ替え
    public void onChange(View view) {
        cng2 = new int[5];
        deck = 10;
        for (int i = 0; i < 5; i++) {
            if (cng[i] == 1) {
                hand_psh[i] = shuffle[deck];
                hand_pn[i] = number[deck];
                hand_ps[i] = suit[deck];
                hand_pi[i] = trump[deck];
                deck++;
            }
            cng[i] = 0;
        }
        p1.setImageResource(hand_pi[0]);
        p2.setImageResource(hand_pi[1]);
        p3.setImageResource(hand_pi[2]);
        p4.setImageResource(hand_pi[3]);
        p5.setImageResource(hand_pi[4]);

        change.setEnabled(false);
        p1.setEnabled(false);
        p2.setEnabled(false);
        p3.setEnabled(false);
        p4.setEnabled(false);
        p5.setEnabled(false);

        onCheck(view);
    }

    // 役の判断
    public void onCheck(View view) {
        int change = 0;
        // 昇順にそれぞれ整列
        Arrays.sort(hand_dsh);
        Arrays.sort(hand_ds);
        for (int i = 0; i < 5; i++) {
            hand_dn[i] = (hand_dsh[i] % 13) + 1;
            hand_di[i] = illust[hand_dsh[i]];
        }
        com_d = hand_dn;
        for (int i = 0; i < 5; i++) {
            if (com_d[i] == 1) {
                com_d[i] = 14;
            }
        }
        Arrays.sort(com_d);

        // 役の判断
        kind_d = 0;
        diff_kd = 0;
        high_kd = 0;
        fls_d = 0;
        st_d = 1;
        pair_d = 0;
        cnt_d = 0;
        diff_pd = new int[2];
        diff_pd[0] = 0;
        diff_pd[1] = 0;
        pnt_d = 0;

        for (int i = 0; i < 4; i++) {
            if (com_d[i] == com_d[i + 1]) {
                kind_d++;
                if (diff_pd[0] == 0 || diff_pd[0] != com_d[i]) {
                    pair_d++;
                    diff_pd[cnt_d] = com_d[i];
                    // ↑でArrayIndexOutOfBoundsExceptionが一回発生したものの再現できず
                    cnt_d++;
                    Log.d("ペア", "pd0" + diff_pd[0] + "pd1" + diff_pd[1] + "cnt" + cnt_d);
                }
                if (high_kd < kind_d) {
                    high_kd = kind_d;
                    diff_kd = com_d[i];
                }
            } else {
                kind_d = 0;
            }
            if (com_d[i] + 1 != com_d[i + 1]) {
                st_d = 0;
            }
            if (hand_ds[i].equals(hand_ds[i + 1])) {
                fls_d++;
            }
        }
        if (com_d[0] == 2 && com_d[1] == 3 && com_d[2] == 4
                && com_d[3] == 5 && com_d[4] == 14) {
            st_d = 1;
        }

        // 配点
        if (fls_d == 4 && st_d == 1 && com_d[0] == 10) {
            pnt_d = 9;
        } else if (fls_d == 4 && st_d == 1) {
            pnt_d = 8;
        } else if (high_kd == 3) {
            pnt_d = 7;
        } else if (high_kd == 2 && pair_d == 2) {
            pnt_d = 6;
        } else if (fls_d == 4) {
            pnt_d = 5;
        } else if (st_d == 1) {
            pnt_d = 4;
        } else if (high_kd == 2) {
            pnt_d = 3;
        } else if (pair_d == 2) {
            pnt_d = 2;
        } else if (pair_d == 1) {
            pnt_d = 1;
        }
        /*Log.d("ディーラー",
                "フラッシュ" + fls_d + "ストレート" + st_d + "カインド" + high_kd + "ペア" + pair_d);
        Log.d("プレイヤー",
                "フラッシュ" + fls_p + "ストレート" + st_p + "カインド" + high_kp + "ペア" + pair_p);*/

        if (flg2 == 0) {
            // 相手の手札交換
            int max = 0;
            for (int i = 0; i < 5; i++) {
                if (hand_dn[i] > max || hand_dn[i] == 1) {
                    max = hand_dn[i];
                }
            }
            Log.d("手札交換前", "数字" + Arrays.toString(hand_dn));
            Log.d("手札交換前", "スート" + Arrays.toString(hand_ds));

            switch (pnt_d) {
                case 0 :
                    // J以上の札は残す
                    for (int i = 0; i < 4; i++) {
                        if (hand_dn[i] < 11) {
                            hand_dsh[i] = shuffle[deck];
                            hand_dn[i] = number[deck];
                            hand_ds[i] = suit[deck];
                            hand_di[i] = trump[deck];
                            deck++;
                            change++;
                        }
                    }
                    break;
                case 1 :
                    // J以上で手札最強の札も一緒に残す
                    for (int i = 0; i < 4; i++) {
                        if (hand_dn[i] != diff_pd[0] &&
                                (hand_dn[i] != max || (max < 11 && max == 1))) {
                            hand_dsh[i] = shuffle[deck];
                            hand_dn[i] = number[deck];
                            hand_ds[i] = suit[deck];
                            hand_di[i] = trump[deck];
                            deck++;
                            change++;
                        }
                    }
                    break;
                case 2 :
                    // フルハウス狙い
                    for (int i = 0; i < 5; i++) {
                        if (hand_dn[i] != diff_pd[0] && hand_dn[i] != diff_pd[1]) {
                            hand_dsh[i] = shuffle[deck];
                            hand_dn[i] = number[deck];
                            hand_ds[i] = suit[deck];
                            hand_di[i] = trump[deck];
                            deck++;
                            change++;
                        }
                    }
                    break;
                case 3 :
                    // フルハウス狙い
                    for (int i = 0; i < 5; i++) {
                        if (hand_dn[i] != diff_pd[0] && hand_dn[i] == max) {
                            hand_dsh[i] = shuffle[deck];
                            hand_dn[i] = number[deck];
                            hand_ds[i] = suit[deck];
                            hand_di[i] = trump[deck];
                            deck++;
                            change++;
                        }
                    }
                    break;
            }
            Log.d("手札交換後", "数字" + Arrays.toString(hand_dn));
            Log.d("手札交換後", "スート" + Arrays.toString(hand_ds));

            // 二回目のベット
            cnt_b = 0;

            check.setEnabled(true);
            bet.setEnabled(true);
            falled.setEnabled(true);
            check.setText(R.string.check);
            bet.setText(R.string.bet3);

            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(this,
                    "相手は" + change + "枚交換しました\n2回目のベッティングラウンドです",
                    Toast.LENGTH_LONG);
            toast.show();
        } else {
            Arrays.sort(hand_psh);
            Arrays.sort(hand_ps);
            for (int i = 0; i < 5; i++) {
                hand_pn[i] = (hand_psh[i] % 13) + 1;
            }
            com_p = hand_pn;
            for (int i = 0; i < 5; i++) {
                if (com_p[i] == 1) {
                    com_p[i] = 14;
                }
            }
            Arrays.sort(com_p);

            roy_p = 0;
            kind_p = 0;
            diff_kp = 0;
            high_kp = 0;
            fls_p = 0;
            st_p = 1;
            pair_p = 0;
            cnt_p = 0;
            diff_pp = new int[2];
            diff_pp[0] = 0;
            diff_pp[1] = 0;

            for (int i = 0; i < 4; i++) {
                if (com_p[i] == com_p[i + 1]) {
                    kind_p++;
                    if (diff_pp[0] == 0 || diff_pp[0] != com_p[i]) {
                        pair_p++;
                        diff_pp[cnt_p] = com_p[i];
                        cnt_p++;
                    }
                    if (high_kp < kind_p) {
                        high_kp = kind_p;
                        diff_kp = com_p[i];
                    }
                } else {
                    kind_p = 0;
                }
                if (com_p[i] + 1 != com_p[i + 1]) {
                    st_p = 0;
                }
                if (hand_ps[i].equals(hand_ps[i + 1])) {
                    fls_p++;
                }
            }
            if (com_p[0] == 2 && com_p[1] == 3 && com_p[2] == 4
                    && com_p[3] == 5 && com_p[4] == 14) {
                st_p = 1;
            }

            if (fls_p == 4 && st_p == 1 && com_p[0] == 10) {
                pnt_p = 9;
            } else if (fls_p == 4 && st_p == 1) {
                pnt_p = 8;
            } else if (high_kp == 3) {
                pnt_p = 7;
            } else if (high_kp == 2 && pair_p == 2) {
                pnt_p = 6;
            } else if (fls_p == 4) {
                pnt_p = 5;
            } else if (st_p == 1) {
                pnt_p = 4;
            } else if (high_kp == 2) {
                pnt_p = 3;
            } else if (pair_p == 2) {
                pnt_p = 2;
            } else if (pair_p == 1) {
                pnt_p = 1;
            }

            onShowDown(view);
        }
    }

    // 勝敗決め
    public void onShowDown(View view) {
        if (toast != null) {
            toast.cancel();
        }

        d1.setImageResource(hand_di[0]);
        d2.setImageResource(hand_di[1]);
        d3.setImageResource(hand_di[2]);
        d4.setImageResource(hand_di[3]);
        d5.setImageResource(hand_di[4]);

        if (pnt_d < pnt_p) {
            onWin(view);
        } else if (pnt_d > pnt_p) {
            onLose(view);
        } else {
            switch (pnt_d) {
                case 0 :
                    onTai(view);
                    break;
                case 1 :
                    //Log.d("ペアの値", "ディーラー" + diff_pd[0]);
                    //Log.d("ペアの値", "プレイヤー" + diff_pp[0]);

                    if (diff_pd[0] < diff_pp[0]) {
                        onWin(view);
                        return;
                    } else if (diff_pd[0] > diff_pp[0]) {
                        onLose(view);
                        return;
                    }
                    for (int i = 4; i >= 0; i--) {
                        if (com_d[i] == diff_pd[0]) {
                            com_d[i] = 0;
                        }

                        if (com_p[i] == diff_pp[0]) {
                            com_p[i] = 0;
                        }
                    }
                    Arrays.sort(com_d);
                    Arrays.sort(com_p);
                    onTai(view);
                    break;
                case 2 :
                    for (int i = 1; i >= 0; i--) {
                        if (diff_pd[i] < diff_pp[i]) {
                            onWin(view);
                            return;
                        } else if (diff_pd[i] > diff_pp[i]) {
                            onLose(view);
                            return;
                        }
                    }
                    for (int i = 4; i >= 0; i--) {
                        if (com_d[i]  == diff_pd[0] || com_d[i] == diff_pd[1]) {
                            com_d[i] = 0;
                        }

                        if (com_p[i]  == diff_pp[0] || com_p[i] == diff_pp[1]) {
                            com_p[i] = 0;
                        }
                    }
                    Arrays.sort(com_d);
                    Arrays.sort(com_p);
                    onTai(view);
                    break;
                case 3:
                    if (diff_kd < diff_kp) {
                        onWin(view);
                    } else {
                        onLose(view);
                    }
                    break;
                case 4 :
                    if (com_d[0] == 10 || com_p[0] == 10) {
                        if (com_d[0] == 10) {
                            onWin(view);
                        } else if (com_p[0] == 10) {
                            onLose(view);
                        } else {
                            onTai(view);
                        }
                    } else {
                        if (com_d[4] < com_p[4]) {
                            onWin(view);
                        } else if (com_d[4] > com_p[4]) {
                            onLose(view);
                        } else {
                            onTai(view);
                        }
                    }
                    break;
                case 5 :
                    onTai(view);
                    break;
                case 6 :
                    if (diff_kd < diff_kp) {
                        onWin(view);
                    } else {
                        onLose(view);
                    }
                    break;
                case 7 :
                    if (diff_kd < diff_kp) {
                        onWin(view);
                    } else {
                        onLose(view);
                    }
                    break;
                case 8 :
                    if (com_d[4] < com_p[4]) {
                        onWin(view);
                    } else if (com_d[4] > com_p[4]) {
                        onLose(view);
                    } else {
                        onTai(view);
                    }
                    break;
                case 9 :
                    onTai(view);
                    break;
            }
        }
    }

    public void onWin(View view) {
        life_p += pot;
        pot = 0;
        if (life_d < 1) {
            switch (pnt_p) {
                case 0:
                    toast = Toast.makeText(this,
                            "魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    break;
                case 1:
                    toast = Toast.makeText(this,
                            "あなたのワンペア!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 2:
                    toast = Toast.makeText(this,
                            "あなたのツーペア!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 3:
                    toast = Toast.makeText(this,
                            "あなたのスリー・オブ・ア・カインド!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 4:
                    toast = Toast.makeText(this,
                            "あなたのストレート!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 5:
                    toast = Toast.makeText(this,
                            "あなたのフラッシュ!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 6:
                    toast = Toast.makeText(this,
                            "あなたのフルハウス!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 7:
                    toast = Toast.makeText(this,
                            "あなたのフォー・オブ・ア・カインド!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 8:
                    toast = Toast.makeText(this,
                            "あなたのストレートフラッシュ!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
                case 9:
                    toast = Toast.makeText(this,
                            "あなたのロイヤルストレートフラッシュ!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
            }
            start.setText("メニューへ");
        } else {
            switch (pnt_p) {
                case 0:
                    toast = Toast.makeText(this,
                            "あなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 1:
                    toast = Toast.makeText(this,
                            "あなたのワンペア!!\nあなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 2:
                    toast = Toast.makeText(this,
                            "あなたのツーペア!!\nあなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 3:
                    toast = Toast.makeText(this,
                            "あなたの\nスリー・オブ・ア・カインド!!\nあなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 4:
                    toast = Toast.makeText(this,
                            "あなたのストレート!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
                case 5:
                    toast = Toast.makeText(this,
                            "あなたのフラッシュ!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
                case 6:
                    toast = Toast.makeText(this,
                            "あなたのフルハウス!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
                case 7:
                    toast = Toast.makeText(this,
                            "あなたの\nフォー・オブ・ア・カインド!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
                case 8:
                    toast = Toast.makeText(this,
                            "あなたのストレートフラッシュ!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
                case 9:
                    toast = Toast.makeText(this,
                            "あなたの|nロイヤルストレートフラッシュ!!\nあなたの勝ち!!",
                            Toast.LENGTH_LONG);
                    break;
            }
        }
        toast.show();
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        start.setEnabled(true);
    }

    public void onTai(View view) {
        for (int i = 4; i >= 0; i--) {
            if (com_d[i] < com_p[i]) {
                onWin(view);
                return;
            } else if (com_d[i] > com_p[i]) {
                onLose(view);
                return;
            }
        }
        switch (pnt_p) {
            case 0 :
                toast = Toast.makeText(this,
                        "引き分け", Toast.LENGTH_LONG);
                break;
            case 1 :
                toast = Toast.makeText(this,
                        "お互いにワンペア!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 2 :
                toast = Toast.makeText(this,
                        "お互いにツーペア!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 3 :
                toast = Toast.makeText(this,
                        "お互いに\nスリー・オブ・ア・カインド!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 4 :
                toast = Toast.makeText(this,
                        "お互いにストレート!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 5 :
                toast = Toast.makeText(this,
                        "お互いにフラッシュ!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 6 :
                toast = Toast.makeText(this,
                        "お互いにフルハウス!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 7 :
                toast = Toast.makeText(this,
                        "お互いに\nフォー・オブ・ア・カインド!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 8 :
                toast = Toast.makeText(this,
                        "お互いにストレートフラッシュ!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 9 :
                toast = Toast.makeText(this,
                        "お互いに\nロイヤルストレートフラッシュ!!\n引き分け", Toast.LENGTH_LONG);
                break;
        }
        toast.show();
        start.setEnabled(true);
    }

    public void onLose(View view) {
        life_d += pot;
        pot = 0;
        if (life_p < 1) {
            switch (pnt_d) {
                case 0:
                    toast = Toast.makeText(this,
                            "ゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 1:
                    toast = Toast.makeText(this,
                            "相手のワンペア!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 2:
                    toast = Toast.makeText(this,
                            "相手のツーペア!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 3:
                    toast = Toast.makeText(this,
                            "相手の\nスリー・オブ・ア・カインド!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 4:
                    toast = Toast.makeText(this,
                            "相手のストレート!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 5:
                    toast = Toast.makeText(this,
                            "相手のフラッシュ!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 6:
                    toast = Toast.makeText(this,
                            "相手のフルハウス!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 7:
                    toast = Toast.makeText(this,
                            "相手の\nフォー・オブ・ア・カインド!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 8:
                    toast = Toast.makeText(this,
                            "相手のストレートフラッシュ!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
                case 9:
                    toast = Toast.makeText(this,
                            "相手の\nロイヤルストレートフラッシュ!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    break;
            }
            start.setText("メニューへ");
        } else {
            switch (pnt_d) {
                case 0:
                    toast = Toast.makeText(this,
                            "あなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 1:
                    toast = Toast.makeText(this,
                            "相手のワンペア!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 2:
                    toast = Toast.makeText(this,
                            "相手のツーペア!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 3:
                    toast = Toast.makeText(this,
                            "相手の\nスリー・オブ・ア・カインド!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 4:
                    toast = Toast.makeText(this,
                            "相手のストレート!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 5:
                    toast = Toast.makeText(this,
                            "相手のフラッシュ!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 6:
                    toast = Toast.makeText(this,
                            "相手のフルハウス!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 7:
                    toast = Toast.makeText(this,
                            "相手の\nフォー・オブ・ア・カインド!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 8:
                    toast = Toast.makeText(this,
                            "相手のストレートフラッシュ!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 9:
                    toast = Toast.makeText(this,
                            "相手の\nロイヤルストレートフラッシュ!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
            }
        }
        toast.show();
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        start.setEnabled(true);
    }

    public void onReset(View view) {
        if (flg == 0) {
            flg = 1;
            onShuffle(view);
            start.setText(R.string.next);
        } else {
            d1.setImageResource(R.drawable.z01);
            d2.setImageResource(R.drawable.z01);
            d3.setImageResource(R.drawable.z01);
            d4.setImageResource(R.drawable.z01);
            d5.setImageResource(R.drawable.z01);
            p1.setImageResource(R.drawable.z01);
            p2.setImageResource(R.drawable.z01);
            p3.setImageResource(R.drawable.z01);
            p4.setImageResource(R.drawable.z01);
            p5.setImageResource(R.drawable.z01);

            pnt_d = 0;
            pnt_p = 0;
            bet_d = 0;
            bet_p = 0;
            flg = 0;
            flg2 = 0;
            bet_f = 0;
            cnt_b = 0;
            for (int i = 0; i < 5; i++) {
                cng[i] = 0;
            }
            if (toast != null) {
                toast.cancel();
            }

            start.setEnabled(true);
            start.setText(R.string.start);
            check.setText(R.string.check);
            bet.setText(R.string.bet3);
            d_bet.setText(String.valueOf(bet_d));
            p_bet.setText(String.valueOf(bet_p));

            if (life_d < 1 || life_p < 1) {
                // ゲームオーバーになったらメニューに戻る
                // 現在はゲームリセット(仮)
                life_d = 10;
                life_p = 10;
                d_life.setText(String.valueOf(life_d));
                p_life.setText(String.valueOf(life_p));
                start.setText(R.string.start);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        d1 = (ImageView) findViewById(R.id.iv_dealer1);
        d2 = (ImageView) findViewById(R.id.iv_dealer2);
        d3 = (ImageView) findViewById(R.id.iv_dealer3);
        d4 = (ImageView) findViewById(R.id.iv_dealer4);
        d5 = (ImageView) findViewById(R.id.iv_dealer5);
        p1 = (ImageView) findViewById(R.id.iv_player1);
        p2 = (ImageView) findViewById(R.id.iv_player2);
        p3 = (ImageView) findViewById(R.id.iv_player3);
        p4 = (ImageView) findViewById(R.id.iv_player4);
        p5 = (ImageView) findViewById(R.id.iv_player5);
        d_life = (TextView) findViewById(R.id.tv_life_d2);
        p_life = (TextView) findViewById(R.id.tv_life_p2);
        d_bet = (TextView) findViewById(R.id.tv_bet_d2);
        p_bet = (TextView) findViewById(R.id.tv_bet_p2);
        start = (Button) findViewById(R.id.bt_start);
        change = (Button) findViewById(R.id.bt_change);
        check = (Button) findViewById(R.id.bt_check);
        bet = (Button) findViewById(R.id.bt_bet);
        falled = (Button) findViewById(R.id.bt_falled);

        illust = new int[52];
        number = new int[52];
        shuffle = new int[52];
        int trump = 0x7f020033;
        for (int i = 0; i < 52; i++) {
            illust[i] = trump;
            trump++;
        }

        change.setEnabled(false);
        check.setEnabled(false);
        bet.setEnabled(false);
        falled.setEnabled(false);
        p1.setEnabled(false);
        p2.setEnabled(false);
        p3.setEnabled(false);
        p4.setEnabled(false);
        p5.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (toast != null) {
            toast.cancel();
        }
        return true;
    }
}
