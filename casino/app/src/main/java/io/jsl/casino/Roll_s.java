package io.jsl.casino;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by kasaiyuuka on 2015/03/17.
 */
public class Roll_s {

    private final Paint paint = new Paint();

    public Bitmap bitmap1;
    public Bitmap bitmap2;
    public Bitmap bitmap3;

    public Rect rect;

    public Roll_s(Bitmap bitmap1, Bitmap bitmap2, Bitmap bitmap3, int width, int height){

        this.bitmap1 = bitmap1;
        this.bitmap2 = bitmap2;
        this.bitmap3 = bitmap3;

        int left = (width - bitmap1.getWidth()) / 2;
        int top =(height - bitmap1.getHeight()) / 2;
        int right = left + bitmap1.getWidth();
        int bottom = top + bitmap1.getHeight();
        rect = new Rect(left, top, right, bottom);

    }


    public void draw1(Canvas canvas) {
        canvas.drawBitmap(bitmap1, 0, 0, paint);
        canvas.drawBitmap(bitmap2, 0, rect.top, paint);
        canvas.drawBitmap(bitmap3, 0, rect.top*2, paint);}
    public void draw2(Canvas canvas) {
        canvas.drawBitmap(bitmap1, rect.left, 0, paint);
        canvas.drawBitmap(bitmap2, rect.left, rect.top, paint);
        canvas.drawBitmap(bitmap3, rect.left, rect.top*2, paint);
    }
    public void draw3(Canvas canvas) {
        canvas.drawBitmap(bitmap1, rect.left*2, 0, paint);
        canvas.drawBitmap(bitmap2, rect.left*2, rect.top, paint);
        canvas.drawBitmap(bitmap3, rect.left*2, rect.top*2, paint);
    }

    public void anim(Canvas canvas)
    {
        canvas.drawBitmap(bitmap1, rect.left, rect.top, paint);
    }
}