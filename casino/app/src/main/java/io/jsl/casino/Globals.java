package io.jsl.casino;

import android.app.Application;

/**
 * Created by kasaiyuuka on 2015/03/12.
 */
public class Globals extends Application {
    static int stpflg_r = 1;  //止めたかどうかのフラグ
    static int[] stpflg_s = new int[3];  //リールをとめるフラグ
    static int[] btnflg = new int[3];  //ストップボタン処理
    static int[] pic = new int[3];  //絵
    static int picflg[][]=new int[][]{{0,0,0},
            {0,0,0},
            {0,0,0},
            {0,0,0}};
    static int syurui;  //0:コウモリ 1:かぶと 2:スライム 3:へび
    static int iro;     //0:黒 1:赤 2:青
    static int choose_syu;  //種類ボタン選択フラグ
    static int choose_iro;  //色ボタン選択フラグ
    static int coin = 1;  //コイン枚数
    static int keisanflg;  //計算を1度だけにするフラグ
    static int startflg;  //二度押しさせないフラグ
    static int kaisuflg;  //判定を1度だけにするフラグ
    static int Atariflg;
    static int fiever;
    static int bet;    //betした回数を記憶
    static int betflg;    //二度押しさせないフラグ
    static int nukoflag=0;
    static int nuko_syu=0;
    static int refund; //払い戻し
    static int reset=6;//ルーレットのリセットまでの回数
    static int flg_s;
    static int flag=0;

    public static void GlobalsIsAllInit(int flg){
        switch (flg){
            case 1 :
                for (int i = 0; i < stpflg_s.length; i++) {
                    btnflg[i] = 1;
                    stpflg_s[i] = 1;
                }
                betflg = 0;
                coin = 20;
                keisanflg = 1;
                startflg = 1;
                bet = 0;
                refund = 0;
                fiever = 0;
                break;
            case 2 :
                stpflg_r =0;
                choose_syu=0;
                choose_iro=0;
                kaisuflg=0;
                startflg=0;
                syurui=0;
                iro=0;
                refund=0;
                break;
        }
    }
}
