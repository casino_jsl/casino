package io.jsl.casino;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


public class Menu extends ActionBarActivity {

    int flg[];

    public void onPoker(View view) {
        Intent i = new Intent(Menu.this, Poker.class);
        int requestCode = 1;
        startActivityForResult(i, requestCode);
    }

    public void onBJ(View view) {
        Intent i = new Intent(Menu.this, Blackjack.class);
        int requestCode = 2;
        startActivityForResult(i, requestCode);
    }

    public void onSlot(View view) {
        Intent i = new Intent(Menu.this, Slot.class);
        int requestCode = 3;
        startActivityForResult(i, requestCode);
    }

    public void onRoulette(View view) {
        Intent i = new Intent(Menu.this, Roulette.class);
        int requestCode = 4;
        startActivityForResult(i, requestCode);
    }

    public void onEnd(View view) {
        Intent i = new Intent(Menu.this, Ending.class);
        startActivity(i);
        Menu.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        int win = i.getIntExtra("win", 0);
        flg = new int[5];
        String[] casino_s = {"poker", "bj", "slot", "roulette", "diff"};


        switch (requestCode) {
            case 1 :
                if (resultCode == RESULT_OK) {
                    flg[0] = win;
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case 2 :
                if (resultCode == RESULT_OK) {
                    flg[1] = win;
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case 3 :
                if (resultCode == RESULT_OK) {
                    flg[2] = 1;
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case 4 :
                if (resultCode == RESULT_OK) {
                    flg[3] = 1;
                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            default:
                break;
        }

        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();

        Cursor c = db.query("casino_table", casino_s,
                null, null, null, null, null);
        while (c.moveToNext()){
            for (int n = 0; n < 5; n++) {
                if (c.getInt(n) == 1 || n == 4) {
                    flg[n] = c.getInt(n);
                }
            }
        }
        c.close();

        ContentValues insertValues = new ContentValues();
        insertValues.put("poker", flg[0]);
        insertValues.put("bj", flg[1]);
        insertValues.put("slot", flg[2]);
        insertValues.put("roulette", flg[3]);
        insertValues.put("diff", flg[4]);
        db.update("casino_table", insertValues, null, null);

        Log.d("帰ってきたdb", "flg[]" + flg[0] + flg[1] + flg[2] + flg[3] + flg[4]);

        onBrave();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        Log.d("result", "ok" + RESULT_OK + "cancel" + RESULT_CANCELED);

        Intent in = getIntent();
        int flg_a = in.getIntExtra("flg", 0);
        if (flg_a != 1) {
            finish();
        }

        flg = new int[5];
        String[] casino_s = {"poker", "bj", "slot", "roulette", "diff"};
        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();

        Cursor c = db.query("casino_table", casino_s,
                null, null, null, null, null);
        while (c.moveToNext()){
            for (int i = 0; i < 5; i++) {
                if (c.getInt(i) != -1) {
                    flg[i] = c.getInt(i);
                } else {
                    flg[i] = 0;
                }
            }
        }
        c.close();

        Log.d("dbその1", "flg[]" + flg[0] + flg[1] + flg[2] + flg[3] + flg[4]);

        ContentValues insertValues = new ContentValues();
        insertValues.put("poker", flg[0]);
        insertValues.put("bj", flg[1]);
        insertValues.put("slot", flg[2]);
        insertValues.put("roulette", flg[3]);
        insertValues.put("diff", flg[4]);
        db.update("casino_table", insertValues, null, null);

        db.close();
        onBrave();
    }

    public void onBrave() {
        Resources r = getResources();
        Drawable armor, sword, shield, helm;
        if (flg[0] == 1) {
            armor = r.getDrawable(R.drawable.armor);
        } else {
            armor = r.getDrawable(R.drawable.brave);
        }
        sword = r.getDrawable(R.drawable.sword);
        if (flg[1] == 1) {
            sword.setAlpha(255);
        } else {
            sword.setAlpha(0);
        }
        shield = r.getDrawable(R.drawable.shield);
        if (flg[2] == 1) {
            shield.setAlpha(255);
        } else {
            shield.setAlpha(0);
        }
        helm = r.getDrawable(R.drawable.helm);
        if (flg[3] == 1) {
            helm.setAlpha(255);
        } else {
            helm.setAlpha(0);
        }
        Drawable[] layers = {armor, sword, shield, helm};
        LayerDrawable ld = new LayerDrawable(layers);

        ImageView brave = (ImageView) findViewById(R.id.brave);
        brave.setImageDrawable(ld);
    }
}
