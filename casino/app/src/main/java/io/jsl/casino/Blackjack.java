package io.jsl.casino;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by tohru on 2015/03/24.
 */
public class Blackjack extends ActionBarActivity {

    int score_p = 0, score_d = 0, life_p = 10, life_d = 10, card_d, card_p, h_cnt, bj = 0, bst = 0,
            coin_b = 1, pot = 0, win, illust[], trump[], score[], shuffle[];

    ImageView d1, d2, d3, d4, d5, d6, d7, d8, d9,
            p1, p2, p3, p4, p5, p6, p7, p8, p9;
    TextView d_life, p_life, point_d, point_p, d_bet, p_bet;
    Button start, next, bet, hit, stand;
    Toast toast;

    public void onBet(View view) {
        start.setEnabled(false);
        bet.setEnabled(true);

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, "ベットしてください", Toast.LENGTH_LONG);
        toast.show();
    }

    // 山札をタッチするとシャッフルするメソッド
    public void onShuffle() {
        int ran1, ran2, buff;
        Random rnd1 = new Random(), rnd2 = new Random();
        trump = new int[52];
        for (int i = 0; i < 52; i++) {
            shuffle[i] = i;
        }

        // ランダムに値を入れ替える(シャッフル)
        for (int i = 0; i < 200; i++) {
            ran1 = rnd1.nextInt(52);
            ran2 = rnd2.nextInt(52);

            buff = shuffle[ran1];
            shuffle[ran1] = shuffle[ran2];
            shuffle[ran2] = buff;
        }

        // シャッフルした順番通りに配列に画像と得点を代入
        for (int i = 0; i < 52; i++) {
            trump[i] = illust[shuffle[i]];
            score[i] = (shuffle[i]) % 13 + 1;
            if (score[i] == 1) {
                score[i] = 11;
            } else if (score[i] > 10) {
                score[i] = 10;
            }
        }

        /*for (String str : cards) {
            Log.d("cards" ,str + ",");
        }*/
        onDraw();
    }

    // 山札から2枚ずつ引くメソッド
    public void onDraw() {
        score_d += score[0];
        p1.setImageResource(trump[2]);
        score_d += score[1];
        d2.setImageResource(trump[1]);
        score_p += score[2];
        p2.setImageResource(trump[3]);
        score_p += score[3];

        point_p.setText(String.valueOf(score_p));
        card_d = 2;
        card_p = 2;
        h_cnt = 4;

        // ナチュラルブラックジャック(2.5倍)
        if (score_d == 21 || score_p == 21) {
            d1.setImageResource(trump[0]);
            point_d.setText(String.valueOf(score_d));

            bj = 2;
            if (score_d != 21 && score_p == 21) {
                onWin();
            } else if (score_d == 21 && score_p == 21) {
                onTie();
            } else if (score_d == 21 && score_p != 21) {
                onLose();
            }

            return;
        }

        hit.setEnabled(true);
        stand.setEnabled(true);
    }

    public void onHit(View view) {
        card_p++;
        h_cnt++;
        if (score[h_cnt - 1] == 11 && score_p + 11 > 21) {
            score[h_cnt - 1] = 1;
        }
        switch (card_p) {
            case 3:
                p3.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 4:
                p4.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 5:
                p5.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 6:
                p6.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 7:
                p7.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 8:
                p8.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 9:
                p9.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
        }

        point_p.setText(String.valueOf(score_p));

        if (score_p > 21) {
            d1.setImageResource(trump[0]);
            point_d.setText(String.valueOf(score_d));
            bst = 1;
            onLose();
        }
    }

    public void onStand(View view) {
        d1.setImageResource(trump[0]);
        point_d.setText(String.valueOf(score_d));

        while (score_d < 17) {
            h_cnt++;
            card_d++;
            if (score[h_cnt - 1] == 11 && score_d + 11 > 21) {
                score[h_cnt - 1] = 1;
            }
            switch (card_d) {
                case 3:
                    d3.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 4:
                    d4.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 5:
                    d5.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 6:
                    d6.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 7:
                    d7.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 8:
                    d8.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 9:
                    d9.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
            }

            if (score_d > 21) {
                bst = 1;
                point_d.setText(String.valueOf(score_d));
                onWin();
                return;
            }
        }

        point_d.setText(String.valueOf(score_d));

        if (score_d == 21 || score_p == 21) {
            bj = 1;
        }

        if (score_d < score_p) {
            onWin();
        } else if (score_d == score_p) {
            onTie();
        } else {
            onLose();
        }
    }

    // 勝利時の処理
    public void onWin() {
        if (toast != null) {
            toast.cancel();
        }
        life_p += pot;

        // ナチュラルブラックジャックだった場合
        if (bj == 2) {
            life_d -= pot;
            life_p += pot * 2;
        }
        pot = 0;
        // ディーラーのライフが無くなった場合
        if (life_d < 1) {
            win = 1;
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        toast = Toast.makeText(this,
                                "魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    } else {
                        toast = Toast.makeText(this,
                                "相手はバストだ!!\n魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    toast = Toast.makeText(this,
                            "あなたのブラックジャック!!\n魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    toast = Toast.makeText(this,
                            "あなたのナチュラルブラックジャック!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
            }
            next.setText("メニューへ");
            toast.show();
        } else {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        toast = Toast.makeText(this,
                                "あなたの勝ち!!", Toast.LENGTH_LONG);
                    } else {
                        toast = Toast.makeText(this,
                                "相手はバストだ!!\nあなたの勝ち!!", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    toast = Toast.makeText(this,
                            "あなたのブラックジャック!!あなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    toast = Toast.makeText(this,
                            "あなたのナチュラルブラックジャック!!あなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
            }
            toast.show();
        }
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onLose() {
        if (toast != null) {
            toast.cancel();
        }
        life_d += pot;

        // ナチュラルブラックジャックだった場合
        if (bj == 2) {
            life_d += pot * 2;
            life_p -= pot;
        }
        pot = 0;
        // プレイヤーのライフが無くなった場合
        if (life_p < 1) {
            win = 0;
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        toast = Toast.makeText(this,
                                "ゲームオーバー…もう一回挑戦だ!",
                                Toast.LENGTH_LONG);
                    } else {
                        toast = Toast.makeText(this,
                                "あなたはバストしてしまった\nゲームオーバー…もう一回挑戦だ!",
                                Toast.LENGTH_LONG);
                    }
                    next.setText("メニューへ");
                    break;
                case 1 :
                    toast = Toast.makeText(this,
                            "相手のブラックジャック!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    next.setText("メニューへ");
                    break;
                case 2 :
                    toast = Toast.makeText(this,
                            "相手のナチュラルブラックジャック!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    next.setText("メニューへ");
                    break;
            }
            toast.show();
        } else {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        toast = Toast.makeText(this,
                                "あなたの負け…", Toast.LENGTH_LONG);
                    } else {
                        toast = Toast.makeText(this,
                                "あなたはバストしてしまった\nあなたの負け…", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    toast = Toast.makeText(this,
                            "相手のブラックジャック!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    toast = Toast.makeText(this,
                            "相手のナチュラルブラックジャック!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
            }
            toast.show();
        }
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onTie() {
        if (toast != null) {
            toast.cancel();
        }
        switch (bj) {
            case 0:
                toast = Toast.makeText(this,
                        "引き分け", Toast.LENGTH_LONG);
                break;
            case 1:
                toast = Toast.makeText(this,
                        "お互いにブラックジャック!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 2:
                toast = Toast.makeText(this,
                        "お互いにナチュラルブラックジャック!!\n引き分け", Toast.LENGTH_LONG);
                break;
        }
        toast.show();
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onReset(View view) {
        d1.setImageResource(R.drawable.tr_z01);
        d2.setImageResource(R.drawable.tr_z01);
        d3.setImageDrawable(null);
        d4.setImageDrawable(null);
        d5.setImageDrawable(null);
        d6.setImageDrawable(null);
        d7.setImageDrawable(null);
        d8.setImageDrawable(null);
        d9.setImageDrawable(null);
        p1.setImageResource(R.drawable.tr_z01);
        p2.setImageResource(R.drawable.tr_z01);
        p3.setImageDrawable(null);
        p4.setImageDrawable(null);
        p5.setImageDrawable(null);
        p6.setImageDrawable(null);
        p7.setImageDrawable(null);
        p8.setImageDrawable(null);
        p9.setImageDrawable(null);
        d_bet.setText("0");
        p_bet.setText("0");

        start.setEnabled(true);
        next.setEnabled(false);

        score_d = 0;
        score_p = 0;
        bj = 0;
        bst = 0;
        coin_b = 1;

        if (toast != null) {
            toast.cancel();
        }

        point_d.setText("？？");
        point_p.setText("？？");

        if ((life_d < 1 || life_p < 1) && pot == 0) {
            // ゲームオーバーになったらメニューに戻る
            onMenu(view);
        }
    }

    public void onMenu(View view) {
        Intent i = new Intent();
        i.putExtra("win", win);
        setResult(RESULT_OK, i);

        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            Intent i = new Intent();
            Bundle b = new Bundle();
            b.putInt("win", win);
            setResult(RESULT_CANCELED, i);

            finish();
            return super.onKeyDown(keyCode, event);
        }else{
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blackjack);

        d1 = (ImageView) findViewById(R.id.iv_dealer1);
        d2 = (ImageView) findViewById(R.id.iv_dealer2);
        d3 = (ImageView) findViewById(R.id.iv_dealer3);
        d4 = (ImageView) findViewById(R.id.iv_dealer4);
        d5 = (ImageView) findViewById(R.id.iv_dealer5);
        d6 = (ImageView) findViewById(R.id.iv_dealer6);
        d7 = (ImageView) findViewById(R.id.iv_dealer7);
        d8 = (ImageView) findViewById(R.id.iv_dealer8);
        d9 = (ImageView) findViewById(R.id.iv_dealer9);
        p1 = (ImageView) findViewById(R.id.iv_player1);
        p2 = (ImageView) findViewById(R.id.iv_player2);
        p3 = (ImageView) findViewById(R.id.iv_player3);
        p4 = (ImageView) findViewById(R.id.iv_player4);
        p5 = (ImageView) findViewById(R.id.iv_player5);
        p6 = (ImageView) findViewById(R.id.iv_player6);
        p7 = (ImageView) findViewById(R.id.iv_player7);
        p8 = (ImageView) findViewById(R.id.iv_player8);
        p9 = (ImageView) findViewById(R.id.iv_player9);
        point_d = (TextView) findViewById(R.id.tv_point_d2);
        point_p = (TextView) findViewById(R.id.tv_point_p2);
        d_life = (TextView) findViewById(R.id.tv_life_d2);
        p_life = (TextView) findViewById(R.id.tv_life_p2);
        d_bet = (TextView) findViewById(R.id.tv_bet_d2);
        p_bet = (TextView) findViewById(R.id.tv_bet_p2);
        start = (Button) findViewById(R.id.bt_start);
        next = (Button) findViewById(R.id.bt_next);
        bet = (Button) findViewById(R.id.bt_bet);
        hit = (Button) findViewById(R.id.bt_hit);
        stand = (Button) findViewById(R.id.bt_stand);

        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(false);
        bet.setEnabled(false);
        hit.setEnabled(false);
        stand.setEnabled(false);

        score_d = 0;
        score_p = 0;
        bj = 0;
        bst = 0;

        illust = new int[52];
        shuffle = new int[52];
        score = new int[52];
        int trump = R.drawable.tr_c01;
        for (int i = 0; i < 52; i++) {
            illust[i] = trump;
            trump++;
        }
    }

    public void onCoins(View view) {
        final CharSequence[] coin = { "1", "2", "3", "4", "5"};

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("何枚ベットしますか？");
        ab.setSingleChoiceItems(coin, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                coin_b = which + 1;
            }
        });
        ab.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (toast != null) {
                    toast.cancel();
                }
                toast = Toast.makeText(Blackjack.this,
                        coin_b + "枚賭けました\nカードを配ります", Toast.LENGTH_LONG);
                toast.show();
                pot += coin_b;
                life_p -= coin_b;

                // ディーラーのベット
                pot += coin_b;
                life_d -= coin_b;

                d_bet.setText(String.valueOf(coin_b));
                p_bet.setText(String.valueOf(coin_b));
                d_life.setText(String.valueOf(life_d));
                p_life.setText(String.valueOf(life_p));
                bet.setEnabled(false);

                onShuffle();
            }
        });
        ab.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }
}