package io.jsl.casino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by tohru on 2015/03/27.
 */
public class Title extends ActionBarActivity {

    int flg = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        flg++;
        Intent i = new Intent(Title.this, Story.class);
        i.putExtra("flg", flg);
        startActivity(i);
        Title.this.finish();

        return true;
    }
}