package io.jsl.casino;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by tohru on 2015/03/27.
 */
public class Ending  extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ending);

        DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = dbh.getWritableDatabase();
        String[] casino_s = {"poker", "bj", "slot", "roulette", "diff"};
        int diff = 0;

        Cursor c = db.query("casino_table", casino_s,
                null, null, null, null, null);
        while (c.moveToNext()){
            diff = c.getInt(4);
        }
        c.close();

        Log.d("cup前", "diff" + diff);
        diff++;
        Log.d("cup後", "diff" + diff);

        ContentValues insertValues = new ContentValues();
        insertValues.put("poker", 0);
        insertValues.put("bj", 0);
        insertValues.put("slot", 0);
        insertValues.put("roulette", 0);
        insertValues.put("diff", diff);
        db.update("casino_table", insertValues, null, null);

        Log.d("", "iv" + insertValues);

        db.close();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent i = new Intent(Ending.this, Title.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        Ending.this.finish();

        return true;
    }
}
