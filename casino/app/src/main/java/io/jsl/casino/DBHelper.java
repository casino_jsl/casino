package io.jsl.casino;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tohru on 2015/03/24.
 */
public class DBHelper extends SQLiteOpenHelper {
    static final String DB_NAME = "weapon_flg.db";  // DB名
    static final int DB_VERSION = 17;                // DBのVersion

    // SQL文をStringに保持しておく
    static String FIRST_TABLE = "insert into casino_table (poker, bj, slot, roulette, diff)" +
            "values (-1, -1, -1, -1, -1)";
    static String CREATE_TABLE = "create table casino_table (" +
                "poker int," +
                "bj int," +
                "slot int," +
                "roulette int," +
                "diff int)";
    static final String DROP_TABLE = "drop table casino_table;";

    // コンストラクタ
    // CREATE用のSQLを取得する
    public DBHelper(Context mContext) {
        super(mContext,DB_NAME,null,DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_TABLE);
        db.execSQL(FIRST_TABLE);
    }
}
