package io.jsl.casino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by tohru on 2015/03/24.
 */
public class Splash extends Activity {

    class splashHandler implements Runnable {
        public void run() {
            Intent i = new Intent(getApplication(), Title.class);
            startActivity(i);
            Splash.this.finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        Handler hdl = new Handler();
        hdl.postDelayed(new splashHandler(), 1000);
    }
}
