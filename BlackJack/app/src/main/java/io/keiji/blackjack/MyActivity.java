package io.keiji.blackjack;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class MyActivity extends ActionBarActivity {

    int score_p = 0, score_d = 0, life_p = 10, life_d = 10, card_d, card_p, h_cnt, bj = 0, bst = 0,
            illust[], trump[], score[], shuffle[];

    ImageView d1, d2, d3, d4, d5, d6, d7, d8, d9,
            p1, p2, p3, p4, p5, p6, p7, p8, p9;
    TextView d_life, p_life, point_d, point_p;
    Button start, next, hit, stand;
    Toast result;

    // 山札をタッチするとシャッフルするメソッド
    public void onShuffle(View view) {
        int ran1, ran2, buff;
        Random rnd1 = new Random(), rnd2 = new Random();
        trump = new int[52];
        for (int i = 0; i < 52; i++) {
            shuffle[i] = i;
        }

        start.setEnabled(false);

        // ランダムに値を入れ替える(シャッフル)
        for (int i = 0; i < 200; i++) {
            ran1 = rnd1.nextInt(52);
            ran2 = rnd2.nextInt(52);

            buff = shuffle[ran1];
            shuffle[ran1] = shuffle[ran2];
            shuffle[ran2] = buff;
        }

        // シャッフルした順番通りに配列に画像と得点を代入
        for (int i = 0; i < 52; i++) {
            trump[i] = illust[shuffle[i]];
            score[i] = (shuffle[i]) % 13 + 1;
            if (score[i] > 10) {
                score[i] = 10;
            }
        }

        /*for (String str : cards) {
            Log.d("cards" ,str + ",");
        }*/
        onDraw(view);
    }

    // 山札から2枚ずつ引くメソッド
    public void onDraw(View view) {
        score_d += score[0];
        p1.setImageResource(trump[2]);
        score_d += score[1];
        d2.setImageResource(trump[1]);
        score_p += score[2];
        p2.setImageResource(trump[3]);
        score_p += score[3];

        point_p.setText(String.valueOf(score_p));
        card_d = 2;
        card_p = 2;
        h_cnt = 4;

        hit.setEnabled(true);
        stand.setEnabled(true);

        // ナチュラルブラックジャック(2.5倍)
        if (score_d == 21 || score_p == 21) {
            d1.setImageResource(trump[0]);
            point_d.setText(String.valueOf(score_d));

            bj = 2;
            if (score_d != 21 && score_p == 21) {
                onWin(view);
            } else if (score_d == 21 && score_p == 21) {
                onTie(view);
            } else if (score_d == 21 && score_p != 21) {
                onLose(view);
            }
        }
    }

    public void onHit(View view) {
        card_p++;
        h_cnt++;
        if (score[h_cnt - 1] == 1 && score_p + 11 <= 21) {
            score[h_cnt - 1] = 11;
        }
        switch (card_p) {
            case 3:
                p3.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 4:
                p4.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 5:
                p5.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 6:
                p6.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 7:
                p7.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 8:
                p8.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
            case 9:
                p9.setImageResource(trump[h_cnt - 1]);
                score_p += score[h_cnt - 1];
                break;
        }

        point_p.setText(String.valueOf(score_p));

        if (score_p > 21) {
            d1.setImageResource(trump[0]);
            point_d.setText(String.valueOf(score_d));
            bst = 1;
            onLose(view);
        }
    }

    public void onStand(View view) {
        d1.setImageResource(trump[0]);
        point_d.setText(String.valueOf(score_d));

        while (score_d < 17) {
            h_cnt++;
            card_d++;
            if (score[h_cnt - 1] == 11 && score_d + 11 > 21) {
                score[h_cnt - 1] = 1;
            }
            switch (card_d) {
                case 3:
                    d3.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 4:
                    d4.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 5:
                    d5.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 6:
                    d6.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 7:
                    d7.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 8:
                    d8.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
                case 9:
                    d9.setImageResource(trump[h_cnt - 1]);
                    score_d += score[h_cnt - 1];
                    break;
            }

            if (score_d > 21) {
                bst = 1;
                point_d.setText(String.valueOf(score_d));
                onWin(view);
                return;
            }
        }

        point_d.setText(String.valueOf(score_d));

        if (score_d == 21 || score_p == 21) {
            bj = 1;
        }

        if (score_d < score_p) {
            onWin(view);
        } else if (score_d == score_p) {
            onTie(view);
        } else {
            onLose(view);
        }
    }

    // 勝利時の処理
    public void onWin(View view) {
        life_d--;
        life_p++;

        // ナチュラルブラックジャックだった場合
        if (bj == 2) {
            life_d--;
            life_p++;
        }
        // ディーラーのライフが無くなった場合
        if (life_d <= 0) {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        result = Toast.makeText(this,
                                "魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    } else {
                        result = Toast.makeText(this,
                                "相手はバストだ!!\n魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    result = Toast.makeText(this,
                            "あなたのブラックジャック!!\n魔物をやっつけたぞ!!", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    result = Toast.makeText(this,
                            "あなたのナチュラルブラックジャック!!\n魔物をやっつけたぞ!!",
                            Toast.LENGTH_LONG);
                    break;
            }
            result.show();
        } else {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        result = Toast.makeText(this,
                                "あなたの勝ち!!", Toast.LENGTH_LONG);
                    } else {
                        result = Toast.makeText(this,
                                "相手はバストだ!!\nあなたの勝ち!!", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    result = Toast.makeText(this,
                            "あなたのブラックジャック!!あなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    result = Toast.makeText(this,
                            "あなたのナチュラルブラックジャック!!あなたの勝ち!!", Toast.LENGTH_LONG);
                    break;
            }
            result.show();
        }
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onLose(View view) {
        life_d++;
        life_p--;

        // ナチュラルブラックジャックだった場合
        if (bj == 2) {
            life_d++;
            life_p--;
        }
        // プレイヤーのライフが無くなった場合
        if (life_p <= 0) {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        result = Toast.makeText(this,
                                "ゲームオーバー…もう一回挑戦だ!",
                                Toast.LENGTH_LONG);
                    } else {
                        result = Toast.makeText(this,
                                "あなたはバストしてしまった\nゲームオーバー…もう一回挑戦だ!",
                                Toast.LENGTH_LONG);
                    }
                    next.setText("メニューへ");
                    break;
                case 1 :
                    result = Toast.makeText(this,
                            "相手のブラックジャック!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    next.setText("メニューへ");
                    break;
                case 2 :
                    result = Toast.makeText(this,
                            "相手のナチュラルブラックジャック!!\nゲームオーバー…もう一回挑戦だ!",
                            Toast.LENGTH_LONG);
                    next.setText("メニューへ");
                    break;
            }
            result.show();
        } else {
            switch (bj) {
                case 0 :
                    if (bst != 1) {
                        result = Toast.makeText(this,
                                "あなたの負け…", Toast.LENGTH_LONG);
                    } else {
                        result = Toast.makeText(this,
                                "あなたはバストしてしまった\nあなたの負け…", Toast.LENGTH_LONG);
                    }
                    break;
                case 1 :
                    result = Toast.makeText(this,
                            "相手のブラックジャック!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
                case 2 :
                    result = Toast.makeText(this,
                            "相手のナチュラルブラックジャック!!\nあなたの負け…", Toast.LENGTH_LONG);
                    break;
            }
            result.show();
        }
        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onTie(View view) {
        switch (bj) {
            case 0:
                result = Toast.makeText(this,
                        "引き分け", Toast.LENGTH_LONG);
                break;
            case 1:
                result = Toast.makeText(this,
                        "お互いにブラックジャック!!\n引き分け", Toast.LENGTH_LONG);
                break;
            case 2:
                result = Toast.makeText(this,
                        "お互いにナチュラルブラックジャック!!\n引き分け", Toast.LENGTH_LONG);
                break;
        }
        result.show();
        next.setEnabled(true);
        hit.setEnabled(false);
        stand.setEnabled(false);
    }

    public void onReset(View view) {
        d1.setImageResource(R.drawable.z01);
        d2.setImageResource(R.drawable.z01);
        d3.setImageDrawable(null);
        d4.setImageDrawable(null);
        d5.setImageDrawable(null);
        d6.setImageDrawable(null);
        d7.setImageDrawable(null);
        d8.setImageDrawable(null);
        d9.setImageDrawable(null);
        p1.setImageResource(R.drawable.z01);
        p2.setImageResource(R.drawable.z01);
        p3.setImageDrawable(null);
        p4.setImageDrawable(null);
        p5.setImageDrawable(null);
        p6.setImageDrawable(null);
        p7.setImageDrawable(null);
        p8.setImageDrawable(null);
        p9.setImageDrawable(null);

        start.setEnabled(true);
        next.setEnabled(false);

        score_d = 0;
        score_p = 0;
        bj = 0;
        bst = 0;

        if (result != null) {
            result.cancel();
        }

        point_d.setText("？？");
        point_p.setText("？？");

        if (life_p < 1) {
            // ゲームオーバーになったらメニューに戻る
            // 現在はゲームリセット(仮)
            life_d = 10;
            life_p = 10;
            d_life.setText(String.valueOf(life_d));
            p_life.setText(String.valueOf(life_p));
            next.setText("次のゲームへ");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        d1 = (ImageView) findViewById(R.id.iv_dealer1);
        d2 = (ImageView) findViewById(R.id.iv_dealer2);
        d3 = (ImageView) findViewById(R.id.iv_dealer3);
        d4 = (ImageView) findViewById(R.id.iv_dealer4);
        d5 = (ImageView) findViewById(R.id.iv_dealer5);
        d6 = (ImageView) findViewById(R.id.iv_dealer6);
        d7 = (ImageView) findViewById(R.id.iv_dealer7);
        d8 = (ImageView) findViewById(R.id.iv_dealer8);
        d9 = (ImageView) findViewById(R.id.iv_dealer9);
        p1 = (ImageView) findViewById(R.id.iv_player1);
        p2 = (ImageView) findViewById(R.id.iv_player2);
        p3 = (ImageView) findViewById(R.id.iv_player3);
        p4 = (ImageView) findViewById(R.id.iv_player4);
        p5 = (ImageView) findViewById(R.id.iv_player5);
        p6 = (ImageView) findViewById(R.id.iv_player6);
        p7 = (ImageView) findViewById(R.id.iv_player7);
        p8 = (ImageView) findViewById(R.id.iv_player8);
        p9 = (ImageView) findViewById(R.id.iv_player9);
        point_d = (TextView) findViewById(R.id.tv_point_d2);
        point_p = (TextView) findViewById(R.id.tv_point_p2);
        d_life = (TextView) findViewById(R.id.tv_life_d2);
        p_life = (TextView) findViewById(R.id.tv_life_p2);
        start = (Button) findViewById(R.id.bt_start);
        next = (Button) findViewById(R.id.bt_next);
        hit = (Button) findViewById(R.id.bt_hit);
        stand = (Button) findViewById(R.id.bt_stand);

        d_life.setText(String.valueOf(life_d));
        p_life.setText(String.valueOf(life_p));
        next.setEnabled(false);
        hit.setEnabled(false);
        stand.setEnabled(false);

        score_d = 0;
        score_p = 0;
        bj = 0;
        bst = 0;

        illust = new int[52];
        shuffle = new int[52];
        score = new int[52];
        int trump = 0x7f020033;
        for (int i = 0; i < 52; i++) {
            illust[i] = trump;
            trump++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
