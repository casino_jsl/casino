package com.example.kasaiyuuka.slotgame;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.os.Vibrator;


import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by kasaiyuuka on 2015/03/17.
 */
public class Viewsyori extends SurfaceView implements SurfaceHolder.Callback {

    private Roll roll;

    private static final long VIBRATION_LENGTH_HIT=100;
    private static final long FPS = 50;
    private final Vibrator vibrator;

    int[] gazouL = new int[]{R.drawable.dora, R.drawable.nuko,R.drawable.koumori, R.drawable.sura, R.drawable.koumori,R.drawable.hono,R.drawable.koumori,
                             R.drawable.nezukou,R.drawable.dora,R.drawable.hono,R.drawable.koumori,R.drawable.nuko,R.drawable.hono,R.drawable.koumori,
                             R.drawable.dora,R.drawable.koumori,R.drawable.hono,R.drawable.yurei,R.drawable.nezukou,R.drawable.yurei,R.drawable.koumori};
    int[] gazouM = new int[]{R.drawable.dora, R.drawable.yurei, R.drawable.nezukou, R.drawable.nuko, R.drawable.sura, R.drawable.hono,R.drawable.yurei,
                             R.drawable.nezukou,R.drawable.nuko,R.drawable.yurei,R.drawable.nezukou,R.drawable.dora,R.drawable.sura,R.drawable.nezukou,
                             R.drawable.yurei,R.drawable.nuko,R.drawable.koumori,R.drawable.hono,R.drawable.nezukou,R.drawable.sura,R.drawable.nuko};
    int[] gazouR = new int[]{R.drawable.dora, R.drawable.koumori, R.drawable.nezukou, R.drawable.koumori, R.drawable.yurei, R.drawable.sura,R.drawable.nezukou,
                             R.drawable.koumori,R.drawable.yurei,R.drawable.koumori,R.drawable.hono,R.drawable.koumori,R.drawable.yurei,R.drawable.nezukou,
                             R.drawable.koumori,R.drawable.yurei,R.drawable.sura,R.drawable.nezukou,R.drawable.koumori,R.drawable.yurei,R.drawable.koumori};
    int APic = 0;
    int roll_num=0;

    Random rnd = new Random();


    public Viewsyori(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);

        vibrator=(Vibrator)context.getSystemService(context.VIBRATOR_SERVICE);
        getHolder().addCallback(this);
    }

    private void drawObject(Canvas c) {

        int width = c.getWidth();
        int height = c.getHeight();

        c.drawColor(Color.rgb(152,251,152));
        tuta();
        fiever();

        drawL(c, width, height);
        drawM(c, width, height);
        drawR(c, width, height);

        roll_num=(roll_num+1+21)%21;

        if (Globals.stpflg[0] == 1 && Globals.stpflg[1] == 1 && Globals.stpflg[2] == 1) {
            if (Globals.keisanflg == 0) {
                keisan();
            }
        }
    }

    private DrawThread drawThread;


    private class DrawThread extends Thread {

        private boolean isFinished;


        @Override
        public void run() {
            super.run();

            SurfaceHolder holder = getHolder();
            while (!isFinished) {
                Canvas canvas = holder.lockCanvas();
                if (canvas != null) {
                    drawObject(canvas);
                    holder.unlockCanvasAndPost(canvas);
                }

                try {
                    sleep(1000 / FPS);

                } catch (InterruptedException e) {
                }
            }
        }
    }

    final Handler handler=new Handler();

    public void message(final int i) {
        new Thread(new Runnable() {
            public void run() {
                final TextView tv = (TextView) ((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.textView);
                final TextView coinlabel = (TextView) ((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.coinlabel);
                final ImageView anim = (ImageView) ((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.anim);
                final RelativeLayout layout = (RelativeLayout)((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.layout1);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (i) {
                            case 0:
                                tv.setText("残念…　はずれ");
                                if(Globals.fiever==1){
                                    tv.setText("-fiever終了ー");
                                    Globals.fiever=0;
                                    layout.setBackgroundResource(R.drawable.haikei);
                                }
                                gameover();

                                break;
                            case 1:
                                tv.setText("モンスター討伐！！\n"+Globals.refund+"コインゲット！");
                                coinlabel.setText(String.valueOf(Globals.coin));
                                if(Globals.refund >= 15 && Globals.fiever==0)
                                {
                                    //ふぃーばーフラグをたてる
                                    Globals.fiever=1;

                                    anim.setBackgroundResource(R.drawable.animation);
                                    AnimationDrawable animation = (AnimationDrawable) anim.getBackground();
                                    animation.start();
                                    //バイブさせる
                                    vibrator.vibrate(VIBRATION_LENGTH_HIT);

                                    tv.setText("-!!FIEVER TIME!!-\n高確率で揃うよ！");
                                }
                                break;
                            case 3:
                                tv.setText("もういっかいまわせるよ！！\nstartをおしてね！");

                                break;
                            case 4:
                                tv.setText(Globals.bet+"コインかけてるよ。\nはじめるならstartをおしてね");
                                break;
                            case 5:
                                tv.setText("startをおしてね");
                                break;
                        }
                    }
                });
            }
        }).start();
    }

    public void fiever() {
        new Thread(new Runnable() {
            public void run() {
                final RelativeLayout layout = (RelativeLayout)((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.layout1);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                       if(Globals.fiever==1)
                       {
                            layout.setBackgroundResource(R.drawable.bonus);
                       }
                    }
                });
            }
        }).start();
    }



    public void tuta() {
        new Thread(new Runnable() {
            public void run() {
                final ImageView t_yoko = (ImageView)((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.t_yoko);
                final ImageView t_naname = (ImageView)((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.t_naname);
                final ImageView t_naname2 = (ImageView)((com.example.kasaiyuuka.slotgame.MainActivity) getContext()).findViewById(R.id.t_naname2);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        AnimationDrawable animation2;

                        if(Globals.bet >=1) {
                            if (Globals.startflg == 1){
                                t_yoko.setBackgroundResource(R.drawable.toumei);
                            }else {
                                t_yoko.setBackgroundResource(R.drawable.t_yoko);
                                animation2 = (AnimationDrawable) t_yoko.getBackground();
                                animation2.start();
                            }
                        }
                        if(Globals.bet >=2)
                        {
                            if (Globals.startflg == 1){
                                t_naname.setBackgroundResource(R.drawable.toumei);
                            }else {
                                t_naname.setBackgroundResource(R.drawable.t_naname1);
                                animation2 = (AnimationDrawable) t_naname.getBackground();
                                animation2.start();
                            }

                        }
                        if(Globals.bet >=3)
                        {
                            if (Globals.startflg == 1)
                            {
                                t_naname2.setBackgroundResource(R.drawable.toumei);
                            }else {
                                t_naname2.setBackgroundResource(R.drawable.t_naname2);
                                animation2 = (AnimationDrawable) t_naname2.getBackground();
                                animation2.start();
                            }

                        }
                    }
                });
            }
        }).start();
    }

    public void clear()
    {
        if(Globals.coin >= 30)
        {
            // インスタンス作成
            AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
            // タイトル設定
            adb.setTitle("クリア！");
            // メッセージ設定
            adb.setMessage("道具を手に入れた？");
            // OKボタン設定
            adb.setPositiveButton("OK", null);
            // 表示
            adb.show();
        }
    }

    public void gameover()
    {
        if(Globals.coin < 0)
        {
            // インスタンス作成
            AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
            // タイトル設定
            adb.setTitle("GameOver");
            // メッセージ設定
            adb.setMessage("残念…");
            // OKボタン設定
            adb.setPositiveButton("最初から", null);
            Globals.GlobalsIsAllInit();
            // 表示
            adb.show();
        }
    }

    public void keisan() {
        int suraflg=0;

        if (gazouL[Globals.pic[0]] == gazouM[Globals.pic[1]] && gazouM[Globals.pic[1]] == gazouR[Globals.pic[2]] ) {
            if (gazouL[Globals.pic[0]] == R.drawable.dora) {
                Globals.refund += 15;
            }else if(gazouL[Globals.pic[0]] == R.drawable.sura){
                message(3);
                suraflg=1;
            } else if (gazouL[Globals.pic[0]] == R.drawable.hono||gazouL[Globals.pic[0]] == R.drawable.yurei) {
                Globals.refund +=8;
            } else if (gazouL[Globals.pic[0]] == R.drawable.koumori) {
                Globals.refund +=6;
            }else {
                Globals.refund +=4;
            }
        }else if(gazouL[Globals.pic[0]]==R.drawable.nuko){
            Globals.refund += 1;
            if (gazouM[Globals.pic[1]]==R.drawable.nuko){
                Globals.refund += 2;
            }
        }

        if (Globals.bet >= 2) {
            for (int i = 0; i <= 2; i = i + 2){
                if (gazouL[(Globals.pic[0] + (i-1)+21) % 21] == gazouM[(Globals.pic[1] + (i - 1)+21) % 21] && gazouM[(Globals.pic[1] + (i-1)+21) % 21] == gazouR[(Globals.pic[2] + (i-1)+21) % 21]) {
                    if (gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.dora) {
                        Globals.refund += 15;
                    }else if(gazouL[(Globals.pic[0]+(i-1)+21) % 21] == R.drawable.sura){
                        message(3);
                        suraflg=1;
                    } else if (gazouL[(Globals.pic[0]+ (i-1)+21)%21] == R.drawable.hono || gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.yurei) {
                        Globals.refund += 8;
                    } else if (gazouL[(Globals.pic[0]+ (i-1)+21)%21] == R.drawable.koumori) {
                        Globals.refund += 6;
                    } else {
                        Globals.refund += 4;
                    }
                } else if (gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.nuko) {
                    Globals.refund += 1;
                    if (gazouM[(Globals.pic[1]+ (i-1)+21) % 21] == R.drawable.nuko) {
                        Globals.refund += 2;
                    }
                }
            }
        }

        if (Globals.bet >= 3) {
            for (int i = 0; i <= 2; i = i + 2){
                if (gazouL[(Globals.pic[0] +(i-1)+21) % 21] == gazouM[(Globals.pic[1]) % 21] && gazouM[(Globals.pic[1] ) % 21] == gazouR[(Globals.pic[2]-(i-1)+21) % 21]) {
                    if (gazouL[Globals.pic[0]] == R.drawable.dora) {
                        Globals.refund += 15;
                    }else if(gazouL[(Globals.pic[0]+(i-1)+21) % 21] == R.drawable.sura){
                        message(3);
                        suraflg=1;
                    } else if (gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.hono || gazouL[(Globals.pic[0]+(i-1)+21) % 21] == R.drawable.yurei) {
                        Globals.refund += 8;
                    } else if (gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.koumori) {
                        Globals.refund += 6;
                    } else {
                        Globals.refund += 4;
                    }
                } else if (gazouL[(Globals.pic[0]+ (i-1)+21) % 21] == R.drawable.nuko) {
                    Globals.refund += 1;
                    if (gazouM[Globals.pic[1]] == R.drawable.nuko) {
                        Globals.refund += 2;
                    }
                }
            }
        }
        Globals.keisanflg = 1;
        if(suraflg==1){
            Globals.startflg=0;
        }else{
            Globals.betflg = 0;
            Globals.bet=0;
        }

        //コイン処理
        if (Globals.refund==0&&suraflg==0) {
            message(0);
        }else {
            message(1);
            Globals.coin += Globals.refund;
        }
    }


    public void startDrawThread() {
        stopDrawThread();

        drawThread = new DrawThread();
        drawThread.start();

    }

    public boolean stopDrawThread() {
        if (drawThread == null) {
            return false;
        }

        drawThread.isFinished = true;
        drawThread = null;
        return true;
    }

    public void drawL( Canvas c,int width,int height) {

        if (Globals.stpflg[0] == 1) {
            if (Globals.Atariflg == 1) {
                APic = gazouL[Globals.pic[0]];
            }
        }else {
            Globals.pic[0] = roll_num;
        }

        Bitmap MBitmap1 = BitmapFactory.decodeResource(getResources(), gazouL[(Globals.pic[0]-1+21)%21]);
        Bitmap MBitmap2 = BitmapFactory.decodeResource(getResources(), gazouL[Globals.pic[0]]);
        Bitmap MBitmap3 = BitmapFactory.decodeResource(getResources(), gazouL[(Globals.pic[0]+1+21)%21]);
        roll = new Roll(MBitmap1,MBitmap2,MBitmap3, width, height);
        roll.draw1(c);
    }

    public void drawM( Canvas c,int width,int height) {
        if (Globals.stpflg[1] == 1) {
            if (Globals.Atariflg == 1) {
                Globals.pic[1] = atari(0, APic);
            }
        }else {
            Globals.pic[1]=roll_num;
        }

        Bitmap MBitmap1 = BitmapFactory.decodeResource(getResources(), gazouM[(Globals.pic[1]-1+21)%21]);
        Bitmap MBitmap2 = BitmapFactory.decodeResource(getResources(), gazouM[Globals.pic[1]]);
        Bitmap MBitmap3 = BitmapFactory.decodeResource(getResources(), gazouM[(Globals.pic[1]+1+21)%21]);
        roll = new Roll(MBitmap1,MBitmap2,MBitmap3, width, height);
        roll.draw2(c);
    }

    public void drawR( Canvas c,int width,int height) {
        if (Globals.stpflg[2] == 1) {
            if (Globals.Atariflg == 1) {
                Globals.pic[2]=atari(1,APic);
            }
        }else {
            Globals.pic[2] = roll_num;
        }

        Bitmap MBitmap1 = BitmapFactory.decodeResource(getResources(), gazouR[(Globals.pic[2]-1+21)%21]);
        Bitmap MBitmap2 = BitmapFactory.decodeResource(getResources(), gazouR[Globals.pic[2]]);
        Bitmap MBitmap3 = BitmapFactory.decodeResource(getResources(), gazouR[(Globals.pic[2]+1+21)%21]);
        roll = new Roll(MBitmap1,MBitmap2,MBitmap3, width, height);
        roll.draw3(c);
    }


    public int atari(int i,int apic)
    {
        int atari=0;
        if (apic == R.drawable.dora){
            if (i==0){atari = 0;}
            if (i==1){atari = 0;}
        }else if(apic == R.drawable.koumori){
            if (i==0){atari = 16;}
            if (i==1){atari = 1;}
        }else if(apic == R.drawable.nezukou) {
            if (i == 0) {atari = 2;}
            if (i == 1) {atari = 2;}
        }else if(apic == R.drawable.sura)
        {
            if (i==0){atari = 4;}
            if (i==1){atari = 5;}
        }else if(apic == R.drawable.yurei)
        {
            if (i==0){atari = 1;}
            if (i==1){atari = 4;}
        }else if(apic == R.drawable.hono)
        {
            if (i==0){atari = 5;}
            if (i==1){atari = 10;}
        }else if(apic == R.drawable.nuko)
        {
            if (i==0){atari = 3;}
            if (i==1){atari = 0;}
        }

    return atari;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startDrawThread();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopDrawThread();
    }
}







