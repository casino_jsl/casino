package com.example.kasaiyuuka.roulette;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity implements View.OnClickListener {

    private Globals globals;
    private TextView coinlabel;
    private ImageView nuko;
    private RelativeLayout layout;
    private android.os.Handler Handler = new Handler();
    private Random rnd = new Random();
    private MainTimerTask mainTimerTask;
    private Timer mainTimer;

    private String syu[]= new String[]{"コウモリ", "カブトムシ", "スライム", "ヘビ"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Globals globals;

        setContentView(R.layout.activity_main);

        Button stop =(Button)findViewById(R.id.stop);
        ImageButton kou_b = (ImageButton) findViewById(R.id.kou_btn);
        ImageButton kabu_b = (ImageButton) findViewById(R.id.kabu_btn);
        ImageButton hebi_b = (ImageButton) findViewById(R.id.hebi_btn);
        ImageButton sura_b = (ImageButton) findViewById(R.id.sura_btn);
        Button red_b = (Button) findViewById(R.id.red_btn);
        Button blk_b = (Button) findViewById(R.id.blk_btn);
        Button blu_b = (Button) findViewById(R.id.blu_btn);
        ImageButton ko_blu = (ImageButton) findViewById(R.id.kou_blu_m);
        ImageButton ko_red = (ImageButton) findViewById(R.id.kou_red_m);
        ImageButton ko_blk = (ImageButton) findViewById(R.id.kou_blk_m);
        ImageButton ka_blu = (ImageButton) findViewById(R.id.kabu_blu_m);
        ImageButton ka_red = (ImageButton) findViewById(R.id.kabu_red_m);
        ImageButton ka_blk = (ImageButton) findViewById(R.id.kabu_blk_m);
        ImageButton su_ble = (ImageButton) findViewById(R.id.sura_blu_m);
        ImageButton su_red = (ImageButton) findViewById(R.id.sura_red_m);
        ImageButton su_blk = (ImageButton) findViewById(R.id.sura_blk_m);
        ImageButton he_blu = (ImageButton) findViewById(R.id.hebi_blu_m);
        ImageButton he_red = (ImageButton) findViewById(R.id.hebi_red_m);
        ImageButton he_blk = (ImageButton) findViewById(R.id.hebi_blk_m);
        this.nuko = (ImageView) findViewById(R.id.nuko);
        this.layout = (RelativeLayout)findViewById(R.id.layout);

        kou_b.setOnClickListener(this);
        hebi_b.setOnClickListener(this);
        sura_b.setOnClickListener(this);
        kabu_b.setOnClickListener(this);
        red_b.setOnClickListener(this);
        blu_b.setOnClickListener(this);
        blk_b.setOnClickListener(this);
        ko_blk.setOnClickListener(this);
        ko_red.setOnClickListener(this);
        ko_blu.setOnClickListener(this);
        ka_blk.setOnClickListener(this);
        ka_red.setOnClickListener(this);
        ka_blu.setOnClickListener(this);
        su_blk.setOnClickListener(this);
        su_red.setOnClickListener(this);
        su_ble.setOnClickListener(this);
        he_blk.setOnClickListener(this);
        he_red.setOnClickListener(this);
        he_blu.setOnClickListener(this);
        stop.setOnClickListener(this);
        nuko.setOnClickListener(this);

        //グローバル変数を取得
        globals = (Globals) this.getApplication();
        //初期化
        globals.GlobalsIsAllInit();

        this.coinlabel = (TextView) findViewById(R.id.coinlabel);
        coinlabel.setText(String.valueOf(globals.coin));

        //タイマーインスタンス生成
        this.mainTimer = new Timer();
        //タスククラスインスタンス生成
        this.mainTimerTask = new MainTimerTask();
        //タイマースケジュール設定＆開始
        this.mainTimer.schedule(mainTimerTask, 1000,4000);
    }

    public void onClick(View v){
        switch (v.getId()) {
            case (R.id.stop):
                if(globals.startflg==1) {
                    globals.stpflg = 1;
                }
                break;

            case(R.id.nuko):
                globals.nukoflag=1;
                globals.nuko_syu=rnd.nextInt(4);

                TextView tv = (TextView) findViewById(R.id.textView);
                tv.setText(syu[globals.nuko_syu]+"がでそうだニャ！");
                break;

            case (R.id.kou_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_syu=1;
                    globals.syurui = 0;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.kabu_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_syu=1;
                    globals.syurui = 1;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.sura_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_syu=1;
                    globals.syurui = 2;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.hebi_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_syu=1;
                    globals.syurui = 3;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));

                }
                break;
            case (R.id.blk_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_iro=1;
                    globals.iro = 0;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.red_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_iro=1;
                    globals.iro = 1;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.blu_btn):
                if(globals.startflg==0) {
                    Globals.GlobalsIsAllInit();
                    globals.choose_iro=1;
                    globals.iro = 2;
                    Globals.startflg=1;

                    globals.coin-=1;
                    coinlabel.setText(String.valueOf(globals.coin));
                }
                break;
            case (R.id.kou_blk_m):
                if(globals.picflg[0][0]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 0;
                        globals.iro = 0;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.kou_red_m):
                if(globals.picflg[0][1]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 0;
                        globals.iro = 1;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.kou_blu_m):
                if(globals.picflg[0][2]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 0;
                        globals.iro = 2;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.kabu_blk_m):
                if(globals.picflg[1][0]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 1;
                        globals.iro = 0;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.kabu_red_m):
                if(globals.picflg[1][1]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 1;
                        globals.iro = 1;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.kabu_blu_m):
                if(globals.picflg[1][2]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 1;
                        globals.iro = 2;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.sura_blk_m):
                if(globals.picflg[2][0]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 2;
                        globals.iro = 0;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.sura_red_m):
                if(globals.picflg[2][1]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 2;
                        globals.iro = 1;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.sura_blu_m):
                if(globals.picflg[2][2]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 2;
                        globals.iro = 2;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.hebi_blk_m):
                if(globals.picflg[3][0]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 3;
                        globals.iro = 0;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.hebi_red_m):
                if(globals.picflg[3][1]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 3;
                        globals.iro = 1;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
            case (R.id.hebi_blu_m):
                if(globals.picflg[3][2]==0) {
                    if (globals.startflg == 0) {
                        Globals.GlobalsIsAllInit();
                        globals.syurui = 3;
                        globals.iro = 2;
                        Globals.startflg = 1;

                        globals.coin-=1;
                        coinlabel.setText(String.valueOf(globals.coin));
                    }
                }
                break;
        }
    }

    public class MainTimerTask extends TimerTask {
        @Override
        public void run() {
            Handler.post( new Runnable() {
                public void run() {
                    int nukoflg =rnd.nextInt(4);

                    if(nukoflg==0&&globals.startflg==0) {
                        int tate = rnd.nextInt(layout.getHeight());
                        int yoko = rnd.nextInt(layout.getWidth());

                        nuko.setVisibility(View.VISIBLE);
                        nuko.layout(yoko, tate, yoko+nuko.getWidth(),tate+nuko.getHeight());

                    }else{
                        nuko.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
