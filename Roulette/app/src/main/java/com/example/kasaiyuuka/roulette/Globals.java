package com.example.kasaiyuuka.roulette;

import android.app.Application;

/**
 * Created by kasaiyuuka on 2015/03/12.
 */
public class Globals extends Application {
    static int stpflg=1;  //止めたかどうかのフラグ
    static int choose_syu;  //種類ボタン選択フラグ
    static int choose_iro;  //色ボタン選択フラグ
    static int coin=20;  //コイン枚数

    static int picflg[][]=new int[][]{{0,0,0},
                               {0,0,0},
                               {0,0,0},
                               {0,0,0}};

    static int syurui;  //0:コウモリ 1:かぶと 2:スライム 3:へび
    static int iro;     //0:黒 1:赤 2:青
    static int kaisuflg;  //判定を1度だけにするフラグ
    static int startflg;  //二度押しさせないフラグ
    static int refund;
    static int reset=6;
    static int nukoflag=0;
    static int nuko_syu=0;


    public static void GlobalsIsAllInit(){
        stpflg=0;
        choose_syu=0;
        choose_iro=0;
        kaisuflg=0;
        startflg=0;
        syurui=0;
        iro=0;
        refund=0;
    }
}


