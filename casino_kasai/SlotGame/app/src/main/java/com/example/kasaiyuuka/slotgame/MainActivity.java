package com.example.kasaiyuuka.slotgame;

import android.media.Image;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private Globals globals;

    private Timer mainTimer;					//タイマー用
    private MainTimerTask mainTimerTask;		//タイマタスククラス
    private ImageView nuko;
    private RelativeLayout layout;
    private Handler Handler = new Handler();
    private Random rnd = new Random();
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button s1btn = (Button) findViewById(R.id.slot_1_btn);
        Button s2btn = (Button) findViewById(R.id.slot_2_btn);
        Button s3btn = (Button) findViewById(R.id.slot_3_btn);
        Button startB = (Button) findViewById(R.id.start_btn);
        Button bet = (Button) findViewById(R.id.bet_btn);
        this.nuko = (ImageView) findViewById(R.id.nuko);
        this.layout = (RelativeLayout)findViewById(R.id.layout1);
        this.tv = (TextView) findViewById(R.id.textView);

        s1btn.setOnClickListener(this);
        s2btn.setOnClickListener(this);
        s3btn.setOnClickListener(this);
        startB.setOnClickListener(this);
        bet.setOnClickListener(this);
        nuko.setOnClickListener(this);

        //グローバル変数を取得
        globals = (Globals) this.getApplication();
        //初期化
        globals.GlobalsIsAllInit();


        TextView coinlabel = (TextView) findViewById(R.id.coinlabel);
        coinlabel.setText(String.valueOf(globals.coin));

        //タイマーインスタンス生成
        this.mainTimer = new Timer();
        //タスククラスインスタンス生成
        this.mainTimerTask = new MainTimerTask();
        //タイマースケジュール設定＆開始
        this.mainTimer.schedule(mainTimerTask, 1000,4000);

    }

    public void onClick(View v){

        switch (v.getId()) {
            case (R.id.slot_1_btn):
                if(globals.btnflg[0]==0) {
                    globals.btnflg[1] = 0;
                    globals.stpflg[0] = 1;
                    globals.btnflg[0]=1;
                    tv.setText("中央のボタンを押してね");
                }
                break;
            case (R.id.slot_2_btn):
                if(globals.btnflg[1]==0) {
                    globals.btnflg[2] = 0;
                    globals.stpflg[1] = 1;
                    globals.btnflg[1] = 1;
                    tv.setText("右のボタンを押してね");
                }
                break;
            case (R.id.slot_3_btn):
                if(globals.btnflg[2]==0) {
                    globals.stpflg[2] = 1;
                    globals.btnflg[2] = 1;
                }
                break;

            case (R.id.bet_btn):
                if(globals.betflg==0) {
                    if (globals.bet < 3) {
                        globals.bet += 1;

                        tv.setText(Globals.bet + "コインかけてるよ。\nはじめるならstartをおしてね");
                        globals.coin -= 1;
                        TextView coinlabel = (TextView) findViewById(R.id.coinlabel);
                        coinlabel.setText(String.valueOf(globals.coin));
                        globals.startflg = 0;
                    } else {
                        globals.bet = 3;
                    }
                }
                break;

            case (R.id.start_btn):
                if (globals.startflg == 0) {
                    globals.startflg = 1;
                    globals.betflg =1;
                    globals.refund=0;

                    globals.keisanflg = 0;
                    globals.Atariflg = 0;
                    globals.btnflg[0]=0;

                    for(int i=0;i<3;i++){
                        globals.stpflg[i]=0;
                    }
                    //当たりフラグ設定
                    Random rnd = new Random();
                    if (Globals.fiever==0) {
                        int atari = rnd.nextInt(3);
                        if (atari == 1) {
                            Globals.Atariflg = 1;
                        }
                    }else{
                        int atari2 = rnd.nextInt(4);
                        if(atari2==0)
                        {
                            Globals.Atariflg = 0;
                        }else{
                            Globals.Atariflg=1;
                        }

                    }
                    TextView coinlabel = (TextView) findViewById(R.id.coinlabel);
                    coinlabel.setText(String.valueOf(Globals.coin));
                    tv.setText("スタート！\n左のボタンを押してね");
                }
                break;
            case(R.id.nuko):
                globals.fiever=1;
                tv.setText("FIEVER TIMEだニャ！");
                break;
        }
    }

    public class MainTimerTask extends TimerTask {
        @Override
        public void run() {
            Handler.post( new Runnable() {
                public void run() {
                    int nukoflg =rnd.nextInt(4);

                    if(nukoflg==0) {

                        int tate = rnd.nextInt(layout.getHeight());
                        int yoko = rnd.nextInt(layout.getWidth());

                        nuko.setVisibility(View.VISIBLE);
                        nuko.layout(yoko, tate, yoko+nuko.getWidth(),tate+nuko.getHeight());

                    }else{
                        nuko.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
