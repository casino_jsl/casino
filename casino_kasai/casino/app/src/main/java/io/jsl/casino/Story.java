package io.jsl.casino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;

/**
 * Created by tohru on 2015/03/26.
 */
public class Story extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent i = new Intent(Story.this, Menu.class);
        startActivity(i);
        Story.this.finish();

        return true;
    }
}
