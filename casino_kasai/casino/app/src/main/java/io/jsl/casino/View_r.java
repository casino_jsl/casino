package io.jsl.casino;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by kasaiyuuka on 2015/03/20.
 */
public class View_r extends View{
    public View_r(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
    }

    private Roll_r roll;

    int gazou[][]=new int[][]
            {{R.drawable.kou_blk,R.drawable.kou_red,R.drawable.kou_blu},
                    {R.drawable.kabu_blk,R.drawable.kabu_red,R.drawable.kabu_blu},
                    {R.drawable.sura_blk,R.drawable.sura_red,R.drawable.sura_blu},
                    {R.drawable.hebi_blk,R.drawable.hebi_red,R.drawable.hebi_blu
                    }};

    int sav_syu;
    int sav_col;

    Random rnd = new Random();

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);

        invalidate();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

        int width = c.getWidth();
        int height = c.getHeight();

        if (Globals.startflg == 1 || Globals.stpflg_r == 1) {

            int syurui = rnd.nextInt(4);
            int iro = rnd.nextInt(3);

            if (Globals.stpflg_r == 0) {
                if(Globals.nukoflag==1)
                {
                    sav_syu = Globals.nuko_syu;
                }else {
                    sav_syu = syurui;
                }

                sav_col = iro;
                message(4);
            }

            Bitmap MBitmap = BitmapFactory.decodeResource(getResources(), gazou[sav_syu][sav_col]);
            roll = new Roll_r(MBitmap, width, height);
            roll.draw(c);
        }

        if (Globals.stpflg_r == 1) {
            if (Globals.picflg[sav_syu][sav_col] == 0) {
                if (Globals.kaisuflg == 0) {
                    hantei();
                }
            } else if (Globals.picflg[sav_syu][sav_col] == 1) {
                if (Globals.kaisuflg == 0) {
                    sav_syu = rnd.nextInt(4);
                    sav_col = rnd.nextInt(3);
                }
            }
        }
    }


    public void hantei()
    {
        if(Globals.choose_syu==1){
            if(sav_syu== Globals.syurui){
                Globals.refund = 4;
                message(1);
            }else {
                message(0);
            }
        }else if(Globals.choose_iro==1){
            if(sav_col== Globals.iro){
                Globals.refund = 3;
                message(1);
            }else {
                message(0);
            }
        }else{
            if(sav_col== Globals.iro && sav_syu== Globals.syurui){
                Globals.refund = 12;
                message(1);
            }else {
                message(0);
            }
        }

        Globals.reset -=1;

        Globals.coin+= Globals.refund;



        Globals.startflg=0;
        Globals.kaisuflg=1;

        if(Globals.reset==0) {
            reset();
        }else {
            message(2);
        }
        Globals.nukoflag=0;
    }

    final Handler handler=new Handler();

    public void message(final int i) {
        new Thread(new Runnable() {
            public void run() {
                final TextView tv = (TextView) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.textView);
                final TextView coinlabel = (TextView) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.coinlabel);
                final ImageButton kou_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_blk_m);
                final ImageButton kou_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_red_m);
                final ImageButton kou_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_blu_m);
                final ImageButton kabu_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_blk_m);
                final ImageButton kabu_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_red_m);
                final ImageButton kabu_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_blu_m);
                final ImageButton sura_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_blk_m);
                final ImageButton sura_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_red_m);
                final ImageButton sura_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_blu_m);
                final ImageButton hebi_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_blk_m);
                final ImageButton hebi_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_red_m);
                final ImageButton hebi_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_blu_m);

                final ImageButton[][] btnpic= new ImageButton[][]{{kou_blk,kou_red,kou_blu,},
                        {kabu_blk,kabu_red,kabu_blu},
                        {sura_blk,sura_red,sura_blu},
                        {hebi_blk,hebi_red,hebi_blu}};
                final ImageView anim = (ImageView) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.anim);


                //final RelativeLayout layout = (RelativeLayout)((io.jsl.casino.Roulette) getContext()).findViewById(R.id.layout1);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (i) {
                            case 0:
                                tv.setText("残念…　はずれ\nリセットまであと"+ Globals.reset+"回");
                                gameover();
                                break;
                            case 1:
                                tv.setText("あたり！！"+ Globals.refund+"コインゲット！\nリセットまであと"+ Globals.reset+"回");
                                coinlabel.setText(String.valueOf(Globals.coin));

                                anim.setBackgroundResource(R.drawable.effect);
                                AnimationDrawable animation = (AnimationDrawable) anim.getBackground();
                                animation.start();

                                break;
                            case 2:
                                btnpic[sav_syu][sav_col].setImageResource(R.drawable.batu);
                                Globals.picflg[sav_syu][sav_col]=1;
                                break;
                            case 3:
                                break;
                            case 4:
                                tv.setText("STOPをおしてね");
                                break;
                        }
                    }
                });
            }
        }).start();
    }

    public void reset(){
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<3;j++){
                Globals.picflg[i][j]=0;
                btnpic_syokika(i,j);
            }
        }
        Globals.reset=6;
        Globals.kaisuflg=1;
    }

    public void btnpic_syokika(final int i, final int j) {
        new Thread(new Runnable() {
            public void run() {
                final TextView coinlabel = (TextView) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.coinlabel);
                final ImageButton kou_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_blk_m);
                final ImageButton kou_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_red_m);
                final ImageButton kou_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kou_blu_m);
                final ImageButton kabu_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_blk_m);
                final ImageButton kabu_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_red_m);
                final ImageButton kabu_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.kabu_blu_m);
                final ImageButton sura_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_blk_m);
                final ImageButton sura_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_red_m);
                final ImageButton sura_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.sura_blu_m);
                final ImageButton hebi_blk = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_blk_m);
                final ImageButton hebi_red = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_red_m);
                final ImageButton hebi_blu = (ImageButton) ((io.jsl.casino.Roulette) getContext()).findViewById(R.id.hebi_blu_m);

                final ImageButton[][] btnpic= new ImageButton[][]{{kou_blk,kou_red,kou_blu,},
                        {kabu_blk,kabu_red,kabu_blu},
                        {sura_blk,sura_red,sura_blu},
                        {hebi_blk,hebi_red,hebi_blu}};
                final int gazou[][]=new int[][]
                        {{R.drawable.kou_blk_m,R.drawable.kou_red_m,R.drawable.kou_blu_m},
                                {R.drawable.kabu_blk_m,R.drawable.kabu_red_m,R.drawable.kabu_blu_m},
                                {R.drawable.sura_blk_m,R.drawable.sura_red_m,R.drawable.sura_blue_m},
                                {R.drawable.hebi_blk_m,R.drawable.hebi_red_m,R.drawable.hebi_blu_m
                                }};
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnpic[i][j].setImageResource(gazou[i][j]);
                    }

                });
            }
        }).start();
    }

    public void gameover()
    {
        if(Globals.coin < 0)
        {
            // インスタンス作成
            AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
            // タイトル設定
            adb.setTitle("GameOver");
            // メッセージ設定
            adb.setMessage("残念…");
            // OKボタン設定
            adb.setPositiveButton("最初から", null);
            Globals.GlobalsIsAllInit(2);
            // 表示
            adb.show();
        }
    }
}