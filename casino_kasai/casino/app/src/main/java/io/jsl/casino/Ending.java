package io.jsl.casino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;

/**
 * Created by tohru on 2015/03/27.
 */
public class Ending  extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ending);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent i = new Intent(Ending.this, Title.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
        Ending.this.finish();

        return true;
    }
}
