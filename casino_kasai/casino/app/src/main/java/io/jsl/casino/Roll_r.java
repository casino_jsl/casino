package io.jsl.casino;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by kasaiyuuka on 2015/03/17.
 */
public class Roll_r {

    private final Paint paint = new Paint();

    public Bitmap bitmap;

    public Rect rect;

    public Roll_r(Bitmap bitmap, int width, int height){

        this.bitmap = bitmap;

        int left = (width - bitmap.getWidth()) / 2;
        int top =(height - bitmap.getHeight()) / 2;
        int right = left + bitmap.getWidth();
        int bottom = top + bitmap.getHeight();
        rect = new Rect(left, top, right, bottom);

    }

    public void draw(Canvas canvas) {canvas.drawBitmap(bitmap, rect.left, rect.top, paint);}
}