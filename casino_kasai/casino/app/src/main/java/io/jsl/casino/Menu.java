package io.jsl.casino;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;


public class Menu extends ActionBarActivity {

    public void onPoker(View view) {
        Intent i = new Intent(Menu.this, Poker.class);
        int requestCode = 1;
        startActivityForResult(i, requestCode);
    }

    public void onBJ(View view) {
        Intent i = new Intent(Menu.this, Blackjack.class);
        int requestCode = 2;
        startActivityForResult(i, requestCode);
    }

    public void onSlot(View view) {
        Intent i = new Intent(Menu.this, Slot.class);
        int requestCode = 3;
        startActivityForResult(i, requestCode);
    }

    public void onRoulette(View view) {
        Intent i = new Intent(Menu.this, Roulette.class);
        int requestCode = 4;
        startActivityForResult(i, requestCode);
    }

    public void onEnd(View view) {
        Intent i = new Intent(Menu.this, Ending.class);
        startActivity(i);
        Menu.this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        Bundle b = i.getExtras();

        switch (requestCode) {
            case 1 :
                if (resultCode == RESULT_OK) {

                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case 2 :
                if (resultCode == RESULT_OK) {

                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case 3 :
                if (resultCode == RESULT_OK) {

                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
