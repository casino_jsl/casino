package io.jsl.casino;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;

/**
 * Created by tohru on 2015/03/27.
 */
public class Title extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Intent i = new Intent(Title.this, Story.class);
        startActivity(i);
        Title.this.finish();

        return true;
    }
}